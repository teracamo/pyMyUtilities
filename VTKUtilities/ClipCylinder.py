import numpy.linalg as linalg
import numpy as np
import vtk
import VTKUtilities.Utilities as utils

class CylinderClipper(object):
    def __init__(self):
        super(CylinderClipper, self)
        self.defaultNormal = [0,1,0]

    def SetInputData(self, inPD):
        self.inPD = vtk.vtkPolyData()
        self.inPD.DeepCopy(inPD)

    def SetNormal(self, normal):
        self.normal = normal

    def SetCenter(self, center):
        self.center = center

    def SetLength(self, length):
        self.length = length

    def SetResolution(self, res):
        self.resolution = res

    def SetRadius(self, radius):
        self.radius = radius

    def Update(self):
        def GetCenterOfCell(index):
            c = self.inPD.GetCell(index)
            p = np.zeros(3)
            for i in xrange(c.GetNumberOfPoints()):
                p += np.array(self.inPD.GetPoint(c.GetPointId(i)))

            if c.GetNumberOfPoints() != 0:
                p = p/c.GetNumberOfPoints()
            else:
                p = np.ones(3)*2**32
            return p

        rotateAxis = np.cross(self.defaultNormal, self.normal)
        rotateDegree = np.rad2deg(np.arccos(np.dot(self.defaultNormal,self.normal)/linalg.norm(self.normal)))


        transform = vtk.vtkTransform()
        transform.PostMultiply()
        transform.Translate(-np.array(self.center))
        transform.RotateWXYZ(rotateDegree, rotateAxis)

        cylinder = vtk.vtkCylinderSource()
        cylinder.SetCenter(0,0,0)
        cylinder.SetHeight(self.length)
        cylinder.SetRadius(self.radius)
        cylinder.SetResolution(self.resolution)
        cylinder.Update()

        # Translate polydata to the designated position
        transfilter = vtk.vtkTransformPolyDataFilter()
        transfilter.SetInputConnection(cylinder.GetOutputPort())
        transfilter.SetTransform(transform.GetLinearInverse())
        transfilter.Update()

        # Turn strips into triangles for intersector filter
        trifilter = vtk.vtkTriangleFilter()
        trifilter.SetInputConnection(transfilter.GetOutputPort())
        trifilter.Update()

        # Find intersections and generate a set of new lines
        intersectionfilter = vtk.vtkIntersectionPolyDataFilter()
        intersectionfilter.SetInputData(0, trifilter.GetOutput())
        intersectionfilter.SetInputData(1, self.inPD)
        intersectionfilter.SplitSecondOutputOn()
        intersectionfilter.SplitFirstOutputOn()
        intersectionfilter.Update()

        output = vtk.vtkPolyData()
        output.DeepCopy(intersectionfilter.GetOutput(2))
        print output

        clipfunc = vtk.vtkCylinder()
        clipfunc.SetCenter(0,0,0)
        clipfunc.SetRadius(self.radius)
        clipfunc.SetTransform(transform)

        for i in xrange(output.GetNumberOfCells()):
            l_cellCoord = GetCenterOfCell(i)
            if (clipfunc.EvaluateFunction(l_cellCoord) < 0):
                print output.GetCell(i)
                output.DeleteCell(i)

        print "pret"
        output.RemoveDeletedCells()
        print "t"
        self.clipperTransform = transform
        self.cylinderTransform = transform.GetLinearInverse()
        self.cylinderPD = trifilter.GetOutput()
        self.intersectionPD = intersectionfilter.GetOutput(0)
        self.inPD = intersectionfilter.GetOutput(2)
        self.output = output

    def GetOutput(self):
        return self.output

    def GetCylinderPolyData(self):
        return self.cylinderPD

    def GetCylinderTransform(self):
        return self.cylinderTransform

    def GetClipperTransform(self):
        return self.clipperTransform

    def GetInterSectionPD(self):
        return self.intersectionPD

    def GetSplittedInPD(self):
        return self.inPD


