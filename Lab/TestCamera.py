import vtk
import VTKUtilities.Utilities as utils

def main():
    plane = vtk.vtkPlaneSource()
    plane.SetNormal(1,1,1)
    plane.SetCenter(0,0,0)
    plane.SetResolution(3,3)
    plane.Update()

    planeActor = utils.GetActor(plane.GetOutput())
    planeActor.GetProperty().SetRepresentationToWireframe()

    origin = utils.GetSphereActor([0,0,0])

    renderList = []
    renderList.append(planeActor)
    # renderList.append(origin)

    ren = utils.RenderActors(renderList, [0.1,0.1,0.1])
    # print ren.camera.GetPosition()
    ren.camera.SetFocalPoint([1,0,0])
    ren.camera.ApplyTransform(utils.GetRotationByNormal([0,0,1],[1,1,1]))
    # ren.camera.Zoom(0?.3)
    # print ren.camera.GetPosition()
    ren.Render()

    print ren.camera.GetFocalPoint()

if __name__ == '__main__':
    main()