import vtk
import VTKUtilities.Utilities as utils
import numpy as np
import scipy.optimize as opt
from scipy.spatial import Voronoi, kdtree

def NumpyToVTKPoints(npPoints):
    vtkPoints = vtk.vtkPoints()
    for point in npPoints:
        vtkPoints.InsertNextPoint(list(point))

    return vtkPoints
def VTKPointsToNumpy(vtkPoints):
    npPoint = []
    for i in xrange(vtkPoint.GetNumberOfPoints()):
        npPoint.Insert(vtkPoint.GetPoint(i))

    return np.array(npPoint)


class SeedSeperator(object):
    def __init__(self, inPD):
        self.inPD = inPD
        self._BuildKDTree()
        pass

    def _BuildKDTree(self):
        """
        Build a kdTree from the points on seed surface

        :return:
        """
        self._numberofPoints = self.inPD.GetNumberOfPoints
        self._seedPoints = np.array([self.inPD.GetPoint(i) for i in xrange(_numberofPoints)])
        self._kdTree = kdtree.KDTree(self._seedPoints)
        pass

    def _QueryPoint(self, point):
        """
        Query a point in point data of the seed closest to the input point.

        :param point:   [np.array] 3-vector specifying the queried point
        :return:
        """
        if (len(point) != 3):
            return False
        else:
            return self._kdTree(point)

    def _CalculateCOM(self, inPoints):
        """
        Calculate the center of mass of the input points.

        :return:
        """
        com = np.array([np.sum(inPoints[:, i])/len(inPoints) for i in xrange(len(inPoints[0]))])
        return com


def main():
    seedSurface = utils.Reader2D("../data/Seed_41.stl")
    seedActor = utils.GetActor(seedSurface)
    seedActor.GetProperty().SetOpacity(0.2)
    seedActor.GetProperty().SetColor(0,0,0.5)

    # Get a list of seed points coordinate
    seedPoints = np.array([seedSurface.GetPoint(i) for i in xrange(seedSurface.GetNumberOfPoints())])

    # Build the kd tree
    kd = kdtree.KDTree(seedPoints)

    # Create a set of vertices with voroni
    vor = Voronoi(seedPoints, incremental=True)
    vtkVorVertices = NumpyToVTKPoints(vor.vertices)
    vtkVorVerticesPD = vtk.vtkPolyData()
    vtkVorVerticesPD.SetPoints(vtkVorVertices)
    vtkVorVerticesPD.BuildLinks()

    cellarray = vtk.vtkCellArray()
    for i in vor.ridge_points:
        if list(i).count(-1) > 0:
            print "skip ", i
            continue
        l_line = vtk.vtkLine()
        l_line.GetPointIds().SetId(0, i[0])
        l_line.GetPointIds().SetId(1, i[1])
        cellarray.InsertNextCell(l_line)

    vtkVorVerticesPD.SetLines(cellarray)

    # Render the points as small spheres
    renList = []
    for points in seedPoints:
        seedPointsActor = utils.GetSphereActor(points, color=[0.5,0.5,0], radius=0.05)
        renList.append(seedPointsActor)


    selector = vtk.vtkSelectEnclosedPoints()
    selector.SetInputData(vtkVorVerticesPD)
    selector.SetSurfaceData(seedSurface)
    selector.Update()

    vtkVorVerticesPDActor = utils.GetActor(vtkVorVerticesPD)
    renList.append(vtkVorVerticesPDActor)
    # for i in xrange(vtkVorVerticesPD.GetNumberOfPoints()):
    #     # if selector.IsInside(i) and kd.query(vtkVorVerticesPD.GetPoint(i))[0]:
    #     if True:
    #         vorPointsActor = utils.GetSphereActor(vtkVorVerticesPD.GetPoint(i), color=[0.8,0.2,0.2], radius=0.05)
    #         renList.append(vorPointsActor)


    renList.append(seedActor)

    ren = utils.ActorsRenderer(renList, [0.7,0.7,0.7])
    ren.Render()

    pass

if __name__ == '__main__':
    main()